'use strict';

const pgp = require('pg-promise')({});

const cn = {
    host: 'localhost',
    port: 5432,
    database: 'testDB',
    user: 'postgres',
    password: 'postgrespassword'
};

const db = pgp(cn); // database instance;

// return existing species
export async function getSpecies() {
    return db.any('select * from species');
}

// return existing categories
export async function getCategories() {
    const stories = await db.any('select * from categories');
    return stories.map((s)=>s.category);
}

// return articles for given category
export async function getCategory(category) {
    const categoryData = await db.any('select * from articlecategory where category=$1', [category]);
    return categoryData.map((ac)=>ac.article)   
}

// return articles for given species
export async function getSpecie(species) {
    const speciesData = await db.any('select * from articlespecies where species=$1', [species]);
    return speciesData.map((ac)=>ac.article)   
}

// return story with related categories and species
export async function getStory(story) {
    const foundStory = await db.any(`select * from articles where title=$1`,[story]);
    const categories = await db.any(`select * from articlecategory where article=$1`,[story]);
    const species = await db.any(`select * from articlespecies where article=$1`, [story]);
    return {
        story:foundStory[0],
        categories:categories.map((cat)=>cat.category),
        species: species.map((spec)=>spec.species)
    };
}
