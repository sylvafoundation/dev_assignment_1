# project attempt
# Running:
 1.  set up env for db.js. i.e: host, username, password, and port for a postgres DB
 2. `psql -f tables.sql`
 3.  `npm install`
 4. `npm start`

The project should then be available at localhost:3000. This is bare minimum, and it's likely to be quite janky. I've taken "don't worry too much about the design" to heart. 

I've hosted node projects in the past on Linux servers using nginx as a reverse proxy and systemd for process management. I didn't have any problems, but they were just hobby projects and never under any load. I'd take a serious look at Heroku or some other platform-as-a-service if I was hosting an app today. 

