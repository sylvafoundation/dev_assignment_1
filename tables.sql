DROP DATABASE testDB;
CREATE DATABASE testDB;
DROP TABLE *;

\c testDB;

CREATE TABLE articles (
  title VARCHAR PRIMARY KEY,
  article TEXT
);

CREATE TABLE categories (
  category VARCHAR PRIMARY KEY
);

CREATE TABLE articlecategory (
  id SERIAL PRIMARY KEY,
  category VARCHAR references categories(category),
  article VARCHAR references articles(title)
);

CREATE TABLE species (
  speciesname VARCHAR PRIMARY KEY,
  about TEXT
);

CREATE TABLE articlespecies (
  id SERIAL PRIMARY KEY,
  species VARCHAR references species(speciesname),
  article VARCHAR references articles(title)
);


INSERT INTO articles (title, article)
  VALUES
    ('article', '<p>test</p>'),
    ('article2', '<p>test</p>'),
    ('article3', '<p>test</p>');

INSERT INTO categories (category)
  VALUES
    ('Designers'),
    ('Heritage'),
    ('Science'),
    ('Furniture');

INSERT INTO articlecategory (category, article)
  VALUES
    ('Designers', 'article'),
    ('Heritage', 'article2'),
    ('Science', 'article3'),
    ('Furniture', 'article');

INSERT INTO species (speciesname, about)
  VALUES 
    ('oak', 'An oak'),
    ('willow', 'A willow'),
    ('thorn', 'Thorn'),
    ('ash', 'An ash');

INSERT INTO articlespecies (species, article)
  VALUES
    ('oak', 'article'),
    ('ash', 'article2'),
    ('thorn', 'article3'),
    ('willow', 'article');
